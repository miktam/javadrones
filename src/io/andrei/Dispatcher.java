package io.andrei;

import io.andrei.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import static io.andrei.utils.Helper.log;

public class Dispatcher extends Thread {


    private List<String> messageQueue = new ArrayList();
    private String TAG = Dispatcher.class.getSimpleName();
    private List<Drone> allDrones = new ArrayList<>();

    public List<String> getMessageQueue() {
        return messageQueue;
    }

    public void addDrone(Drone drone) {
        allDrones.add(drone);
    }

    public List<Drone> getDrones() {
        return allDrones;
    }

    @Override
    public void run() {
        try {
            while (true) {
                if (messageQueue.size() == 0) {
                    synchronized (this) {
                        //now go to sleep, save the cycles";
                        wait();
                    }
                }
            }
        } catch (InterruptedException e) {
            log(TAG, "Error: " + e.getMessage());
        }
    }


    /**
     * Send a message to a drones
     *
     * @param message to send
     * @throws InterruptedException
     */
    public synchronized void sendMessage(String message) throws InterruptedException {
        while (messageQueue.size() == Constants.DRONE_MESSAGE_CAPACITY) {
            wait();
        }

        messageQueue.add(message);
        notify();

        // wake up all actors to proceed
        for (Drone drone : allDrones) {

            synchronized (drone) {
                try {
                    drone.notify();
                } catch (Exception e) {
                    log(TAG, "Dispatcher Error: notified error" + e);
                }
            }
        }
    }

    /**
     * Receive a message - used by drones
     *
     * @return received message
     * @throws InterruptedException
     */
    public synchronized String receiveMessage() throws InterruptedException {
        notify();
        while (messageQueue.size() == 0) {
            wait();
        }
        String message = (String) messageQueue.remove(0);
        return message;
    }
}