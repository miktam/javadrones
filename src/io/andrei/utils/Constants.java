package io.andrei.utils;


public interface Constants {

    // in m
    Integer STATION_NEARBY_LIMIT = 350;

    // drone can proceed max to 10 messages
    Integer DRONE_MESSAGE_CAPACITY = 10;

    // commands
    String COMMAND_MOVE = "MOVE";
    String COMMAND_SHUTDOWN = "SHUTDOWN";

    enum TrafficConditions {
        HEAVY, LIGHT, MODERATE
    }
}
