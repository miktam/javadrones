package io.andrei.utils;

import io.andrei.models.DronePosition;
import io.andrei.models.GeoPosition;
import io.andrei.models.TubePosition;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class Helper {

    public final static String FILE_SEPARATOR = ",";
    public final static String COMMAND_SEPARATOR = " ";

    // reduce amount of data - mainly moving of the drones
    public final static Boolean ENABLE_DEBUG_INFO = false;

    private static final String TAG = Helper.class.getSimpleName();

    // useful to move drones faster
    // set to 1 to see real-time
    public final static Integer SPEEDUP_DRONES_MULTI = 100;

    static {
        log(TAG, "[SETTINGS] Current simulation speed: " + SPEEDUP_DRONES_MULTI + "\tYou can change it in Helper.SPEEDUP_DRONES_MULTI");
        log(TAG, "[SETTINGS] Enable debug info: " + ENABLE_DEBUG_INFO + "\tYou can change it in Helper.ENABLE_DEBUG_INFO\n");
    }

    public static void log(String tag, String message) {
        System.out.println(tag + "\t" + message);
    }

    /**
     * Read a file from a path
     *
     * @param path to a file
     * @return Stream of data
     */
    public static Stream readFile(String path) {

        Path pathNative = Paths.get(path);
        try {
            Stream<String> lines = Files.lines(pathNative);
            return lines;
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return null;

    }

    /**
     * Helper method to init tubes
     */
    public static List<TubePosition> createTubePositions(Stream<String> data) {
        List<TubePosition> result = new ArrayList<>();
        data.forEach(tube -> result.add(new TubePosition(tube)));
        return result;
    }

    /**
     * Helper method to init drones
     */
    public static List<DronePosition> createDronePositions(Stream<String> data) {
        List<DronePosition> result = new ArrayList<>();
        data.forEach(dronePos -> {
            result.add(new DronePosition(dronePos));
        });

        return result;
    }

    /**
     * Calculate distance between two geolocation points
     * based on http://stackoverflow.com/questions/639695/how-to-convert-latitude-or-longitude-to-meters
     *
     * @param here point one
     * @param next point two
     * @return distance in meters
     */
    //
    public static Long distance(GeoPosition here, GeoPosition next) {

        Double RAD = 0.000008998719243599958;
        Double res = Math.sqrt(Math.pow(here.lat - next.lat, 2) + Math.pow(here.lon - next.lon, 2)) / RAD;

        return Math.round(res);
    }
}