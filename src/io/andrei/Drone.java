package io.andrei;

import io.andrei.models.*;
import io.andrei.utils.Constants;
import io.andrei.utils.Helper;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static io.andrei.utils.Helper.log;

public class Drone extends Thread {

    public final Integer id;
    public final Dispatcher dispatcher;

    private String TAG = Drone.class.getSimpleName();
    private DronePosition currentPosition;
    private Set<TubePosition> stationsNearby = new HashSet<>();
    private final Lock lock = new ReentrantLock();
    private long speedInMS = 0;

    Drone(Dispatcher dispatcher, Integer id, DronePosition pos) {
        this.dispatcher = dispatcher;
        this.id = id;
        this.currentPosition = pos;
    }

    @Override
    public void run() {
        try {
            while (true) {
                String message = dispatcher.receiveMessage();

                processMessage(message);

                synchronized (this) {
                    try {

                        if (dispatcher.getMessageQueue().size() == 0) {
                            // awoid busy wait
                            wait();
                        }
                    } catch (Exception e) {
                        log(TAG, "Error: exc in actor " + e);
                    }
                }
            }
        } catch (InterruptedException e) {
        }
    }

    /**
     * Parse message, validate it and act accordingly
     * Possible messages:
     * MOVE 5937 51.476761 -0.100064 2011-03-22T07:57:30
     *  drone moves to the given position
     * SHUTDOWN 5937
     *  drone is shuting down
     * @param message to process
     */
    private void processMessage(String message) {

        String command[] = message.split(Helper.COMMAND_SEPARATOR);

        if (command.length < 2) {
            log(TAG, "Error: message should contain at least two parameters");
            return;
        }

        Integer droneAddress = Integer.parseInt(command[1]);

        if (this.id.equals(droneAddress)) {

            switch (command[0]) {
                case Constants.COMMAND_MOVE: {

                    DronePosition nextDronePosition = new DronePosition(Integer.parseInt(command[1]), new GeoPosition(command[2], command[3]), LocalDateTime.parse(command[4], DateTimeFormatter.ISO_LOCAL_DATE_TIME));
                    this.move(nextDronePosition);
                    break;

                }
                case Constants.COMMAND_SHUTDOWN: {

                    dispatcher.getDrones().remove(this);

                    if (Helper.ENABLE_DEBUG_INFO) {
                        log(TAG, "Got SHUTDOWN signal for " + this.id);
                        log(TAG, "# of drones still active: " + dispatcher.getDrones().size());
                    }

                    this.stop();
                }
                default: {
                    log(TAG, "Command not recognized: " + command);
                }
            }
        }

    }

    /**
     * Moves current drone from currentPosition to the nextPosition
     * Drone can not process another commands when moving
     * As soon as drone moves to the new position, check the nearby stations, it can move again
     * @param nextPosition position to move
     */
    private void move(DronePosition nextPosition) {

        LocalTime fromTime = this.currentPosition.time.toLocalTime();
        LocalTime toTime = nextPosition.time.toLocalTime();

        long secondsToMove = (toTime.toSecondOfDay() - fromTime.toSecondOfDay());

        // avoid division by zero
        if (secondsToMove == 0) {
            secondsToMove = 1;
        }

        Long distanceToCover = Helper.distance(this.currentPosition.pos, nextPosition.pos);

        this.speedInMS = Helper.SPEEDUP_DRONES_MULTI * distanceToCover / secondsToMove;

        if (this.speedInMS == 0) {
            this.speedInMS = 1;
        }

        Long timeNeededToMoveThere = distanceToCover / speedInMS;
        if (Helper.ENABLE_DEBUG_INFO) {
            log(TAG, this.id + " is moving, it will take " + timeNeededToMoveThere + "s, with speed " + speedInMS + "m/s");
        }

        // simulate the time which is needed for a drone to get to the new destination point
        lock.lock();
        try {
            Thread.sleep(timeNeededToMoveThere * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }

        this.currentPosition = nextPosition;

        if (Helper.ENABLE_DEBUG_INFO) {
            log(TAG, this.id + " moved successfully for a distance " + distanceToCover + "m");
        }

        isStationNearby();
    }

    private void isStationNearby() {

        List<TubePosition> tubes = DataHolder.getTubes();

        // reset station nearby
        stationsNearby = new HashSet<>();
        tubes.parallelStream().forEach(tube -> {
            Long distance = Helper.distance(this.currentPosition.pos, tube.pos);
            if (distance < Constants.STATION_NEARBY_LIMIT) {

                stationsNearby.add(tube);
            }
        });
    }

    /**
     * @return get Tube Stations nearby
     * @see Constants.STATION_NEARBY_LIMIT
     */
    public Set<TubePosition> getStationsNearby() {
        return stationsNearby;
    }

    /**
     * Report of the traffic conditions made by drone
     *
     * @return Drone ID, Time, Speed, Conditions of Traffic (HEAVY, LIGHT, MODERATE). This can be chosen randomly.
     */
    public TrafficReport getTrafficReport() {
        Constants.TrafficConditions cond = Constants.TrafficConditions.values()[new Random().nextInt(Constants.TrafficConditions.values().length)];
        return new TrafficReport(this.id, this.currentPosition.time, this.speedInMS, cond);
    }

}


