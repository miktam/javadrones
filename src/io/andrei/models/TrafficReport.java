package io.andrei.models;

import java.time.LocalDateTime;

public class TrafficReport {

    //Drone ID, Time, Speed, Conditions of Traffic (HEAVY, LIGHT, MODERATE). This can be chosen randomly.
    public final Integer droneID;
    public final LocalDateTime time;
    public final Long speed;
    public final Enum trafficConditions;

    public TrafficReport(Integer droneID, LocalDateTime time, Long speed, Enum trafficConditions) {
        this.droneID = droneID;
        this.time = time;
        this.speed = speed;
        this.trafficConditions = trafficConditions;
    }

    @Override
    public String toString() {
        return "TrafficReport{" +
                "droneID=" + droneID +
                ", time=" + time +
                ", speed=" + speed + "m/s" +
                ", trafficConditions=" + trafficConditions +
                '}';
    }
}
