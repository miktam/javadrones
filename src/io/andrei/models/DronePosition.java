package io.andrei.models;

import io.andrei.utils.Helper;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * drone id,latitude,longitude,time
 */
public class DronePosition {

    public final Integer id;

    public final GeoPosition pos;
    public final LocalDateTime time;

    public DronePosition(String stringEntry) {
        String tokens[] = stringEntry.split(Helper.FILE_SEPARATOR);
        this.id = Integer.parseInt(tokens[0]);
        this.pos = new GeoPosition(Double.parseDouble(tr(tokens[1])), Double.parseDouble(tr(tokens[2])));

        String date[] = tokens[3].split(" ");
        String newOne = tr(Arrays.stream(date).collect(Collectors.joining("T")));
        this.time = LocalDateTime.parse(newOne, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
    }

    public DronePosition(Integer id, GeoPosition pos, LocalDateTime time) {
        this.id = id;
        this.pos = pos;
        this.time = time;
    }

    private static String tr(String s) {
        return s.substring(1, s.length() - 1);
    }

    public String getCommand() {
        return "MOVE" + Helper.COMMAND_SEPARATOR + id + Helper.COMMAND_SEPARATOR + pos.lat + Helper.COMMAND_SEPARATOR + pos.lon + Helper.COMMAND_SEPARATOR + time;
    }


    @Override
    public String toString() {
        return "DronePosition{" +
                "id=" + id +
                ", pos=" + pos +
                ", time=" + time +
                '}';
    }
}
