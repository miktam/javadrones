package io.andrei.models;

import java.util.ArrayList;
import java.util.List;

public class DataHolder {

    private static List<TubePosition> tubes = new ArrayList<>();

    public static void setTubes(List<TubePosition> tubes) {
        DataHolder.tubes = tubes;
    }

    public static List<TubePosition> getTubes() {
        return tubes;
    }
}
