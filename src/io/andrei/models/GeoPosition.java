package io.andrei.models;

public class GeoPosition {

    public final Double lat;
    public final Double lon;

    public GeoPosition(Double lat, Double lon) {
        this.lat = lat;
        this.lon = lon;
    }

    public GeoPosition(String lat, String lon) {
        this.lat = Double.parseDouble(lat);
        this.lon = Double.parseDouble(lon);
    }

    @Override
    public String toString() {
        return "GeoPosition{" +
                "lat=" + lat +
                ", lon=" + lon +
                '}';
    }
}
