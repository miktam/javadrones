package io.andrei.models;

import io.andrei.utils.Helper;

/**
 * station,lat,lon
 */
public class TubePosition {

    public final String name;
    public final GeoPosition pos;

    public TubePosition(String stringEntry) {
        String tokens[] = stringEntry.split(Helper.FILE_SEPARATOR);
        this.name = tokens[0];
        this.pos = new GeoPosition(Double.parseDouble(tokens[1]), Double.parseDouble(tokens[2]));
    }

    @Override
    public String toString() {
        return "TubePosition{" +
                "name='" + name + '\'' +
                ", pos=" + pos +
                '}';
    }
}
