package io.andrei;

import io.andrei.models.DataHolder;
import io.andrei.models.DronePosition;
import io.andrei.utils.Constants;
import io.andrei.utils.Helper;
import org.junit.Assert;
import org.junit.Test;

import java.time.LocalTime;
import java.util.List;

import static io.andrei.utils.Helper.log;

public class TestDrones {

    static List<DronePosition> drone5937positions;
    static List<DronePosition> drone6043positions;

    private static String TAG = TestDrones.class.getSimpleName();

    @org.junit.BeforeClass
    public static void setUp() throws Exception {
        DataHolder.setTubes(Helper.createTubePositions(Helper.readFile("./data/tube.csv")));
        drone5937positions = Helper.createDronePositions(Helper.readFile("./data/5937.csv"));
        drone6043positions = Helper.createDronePositions(Helper.readFile("./data/6043.csv"));
        log(TAG, "Loaded tubes: " + DataHolder.getTubes().size());
        log(TAG, "Loaded drone5937 positions: " + drone5937positions.size());
        log(TAG, "Loaded drone6043 positions: " + drone6043positions.size());
    }

    @Test
    public void calculateDistanceBetweenFirstDronePositionAndTube() throws InterruptedException {
        Assert.assertFalse("distance should be more than 0 m", Helper.distance(drone5937positions.get(0).pos, DataHolder.getTubes().get(0).pos) <= 0);
    }

    @Test
    public void testTwoDronesWithShutdownForBoth() throws InterruptedException {

        final Dispatcher producer = new Dispatcher();
        producer.start();

        // create first drone and initialize it with the initial location
        Drone drone1 = new Drone(producer, 5937, drone5937positions.get(0));

        // collect this drone in producers list
        synchronized (producer.getDrones()) {
            producer.addDrone(drone1);
        }

        drone1.start();

        // start pumping data
        for (DronePosition d1 : drone5937positions) {

            // if drone is still moving, check the Traffic Report - if any stations are nearby
            if (drone1.getStationsNearby() != null && drone1.getStationsNearby().size() > 0) {
                log(TAG, drone1.getTrafficReport().toString());
            }

            // check if drone is still alive, if yes, move it
            // stop condition is hardcoded (08:10 time)
            if (stillMoving(d1)) {
                producer.sendMessage(d1.getCommand());
                Thread.sleep(10);
            } else {
                // send the signal to kill the drone
                producer.sendMessage(Constants.COMMAND_SHUTDOWN + Helper.COMMAND_SEPARATOR + drone1.id);
                break;
            }
        }

        // prepare separated thread to start pumping data from producer to drone2
        Thread threadFor6043 = new Thread(() -> {

            Drone drone2 = new Drone(producer, 6043, drone6043positions.get(0));
            synchronized (producer.getDrones()) {
                producer.addDrone(drone2);
            }
            drone2.start();

            Assert.assertFalse("drones queue should not be empty", producer.getDrones().size() == 0);

            for (DronePosition d2 : drone6043positions) {

                if (drone2.getStationsNearby() != null && drone2.getStationsNearby().size() > 0) {
                    log(TAG, drone2.getTrafficReport().toString());
                }
                try {

                    if (stillMoving(d2)) {
                        producer.sendMessage(d2.getCommand());
                        Thread.sleep(10);
                    } else {
                        producer.sendMessage(Constants.COMMAND_SHUTDOWN + Helper.COMMAND_SEPARATOR + drone2.id);
                        break;
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }

        });
        threadFor6043.start();

        // wait for this thread, it should stop when it will receive the SHUTDOWN signal
        threadFor6043.join();
        drone1.join();
    }

    @Test
    public void sendShutdownToDrone() throws InterruptedException {

        final Dispatcher producer = new Dispatcher();
        producer.start();
        Drone drone1 = new Drone(producer, 5937, drone5937positions.get(0));
        drone1.start();

        synchronized (producer.getDrones()) {
            producer.addDrone(drone1);
        }

        Assert.assertEquals(producer.getDrones().size(), 1);

        producer.sendMessage(Constants.COMMAND_SHUTDOWN + Helper.COMMAND_SEPARATOR + drone1.id);

        // give a time to proceed the command
        Thread.sleep(1000);

        Assert.assertEquals(producer.getDrones().size(), 0);

        drone1.join();
    }

    /**
     * Emulation should finish @ 08:10, where the drones will receive a "SHUTDOWN" signal.
     */
    private boolean stillMoving(DronePosition entry) {

        LocalTime TIME_TO_FINISH_SIMULATION = LocalTime.parse("08:10");
        if (TIME_TO_FINISH_SIMULATION.isBefore(entry.time.toLocalTime())) {
            return false;
        }
        return true;
    }
}